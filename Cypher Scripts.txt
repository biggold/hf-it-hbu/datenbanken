CREATE Scripts
--------------

Create base graph model

CREATE (Skihuette:Ferienhaus {name: "Skihuette"})<-[:GEBUCHT {von: "2020-01-04", bis: "2020-01-21"}]-(:Kunde {name: "Xavier Feinschmecker"})-[:GEBUCHT {von: "2021-05-30", bis: "2021-06-23"}]->(`Maison du vin`:Ferienhaus {name: "Maison du vin"})-[:STEHT_AUF]->(Malediven:Insel {name: "Malediven"})<-[:STEHT_AUF]-(Chaeshuette:Ferienhaus {name: "Chaeshuette"})<-[:GEBUCHT {von: "2019-05-27", bis: "2019-07-11"}]-(:Kunde {name: "Hubert Schuhbus"})-[:GEBUCHT {von: "2022-04-21", bis: "2022-06-02"}]->(Jugendherberge:Ferienhaus {name: "Jugendherberge"}),
(Chaeshuette)<-[:GEBUCHT {von: "2021-07-09", bis: "2021-07-10"}]-(`Michael Groeninger`:Kunde {name: "Michael Groeninger"})-[:GEBUCHT {von: "2018-04-04", bis: "2018-05-01"}]->(Skihuette)-[:STEHT_AUF]->(Malediven),
(Jungelhuette:Ferienhaus {name: "Jungelhuette"})<-[:GEBUCHT {von: "2020-08-08", bis: "2020-08-09"}]-(`Franz Ferdinant`:Kunde {name: "Franz Ferdinant"})-[:GEBUCHT {von: "2023-06-21", bis: "2024-07-11"}]->(`Maison de Soleil`:Ferienhaus {name: "Maison de Soleil"})-[:STEHT_AUF]->(:Insel {name: "Hawaii"}),
(:Kunde {name: "Petra Buergi"})-[:GEBUCHT {von: "2016-04-08", bis: "2016-04-20"}]->(Jungelhuette)-[:STEHT_AUF]->(:Insel {name: "Madagaskar"})<-[:STEHT_AUF]-(Baumhaus:Ferienhaus {name: "Baumhaus"})<-[:GEBUCHT {von: "2000-08-08", bis: "2001-01-01"}]-(:Kunde {name: "Tom Bombadil"})-[:GEBUCHT {von: "2020-11-11", bis: "2020-11-12"}]->(Jungelhuette)<-[:GEBUCHT {von: "2009-04-04", bis: "2009-09-09"}]-(:Kunde {name: "Tim Schmecker"})-[:GEBUCHT {von: "2012-12-12", bis: "2012-12-24"}]->(Baumhaus)<-[:GEBUCHT {von: "2020-12-05", bis: "2021-01-04"}]-(:Kunde {name: "Gerda Kried"}),
(:Kunde {name: "Klaus Guentherd"})-[:GEBUCHT {von: "2022-07-02", bis: "2022-08-08"}]->(Jugendherberge)-[:STEHT_AUF]->(:Insel {name: "Ibiza"})<-[:STEHT_AUF]-(Strandhaus:Ferienhaus {name: "Strandhaus"})<-[:GEBUCHT {von: "2022-09-09", bis: "2022-12-21"}]-(:Kunde {name: "Gerda Jaeggi "}),
(`Franz Ferdinant`)-[:GEBUCHT {von: "2021-12-12", bis: "2021-12-15"}]->(Skihuette),
(:Kunde {name: "Franziska Ferdinant"})-[:GEBUCHT {von: "2013-12-02", bis: "2013-12-22"}]->(`Maison de Soleil`)<-[:GEBUCHT {von: "2015-01-01", bis: "2015-02-12"}]-(:Kunde {name: "Juerg Schlosser"})-[:GEBUCHT {von: "2015-01-01", bis: "2015-02-12"}]->(`Maison du vin`),
(:Kunde {name: "Herbert Brot"})-[:GEBUCHT {von: "2017-03-03", bis: "2017-04-04"}]->(Jungelhuette)<-[:GEBUCHT {von: "2021-09-23", bis: "2021-09-24"}]-(`Michael Groeninger`),
(Strandhaus)<-[:GEBUCHT {von: "2020-11-11", bis: "2021-01-22"}]-(:Kunde {name: "Valeri Baum"})-[:GEBUCHT {von: "2021-11-11", bis: "2021-12-12"}]->(Jugendherberge)

Create new Kunde (returning variables is optional)
MATCH (f:Ferienhaus{name: "Baumhaus"})
CREATE (k:Kunde {name: "testKunde"})-[:GEBUCHT {von: "1900-01-31", bis: "1900-01-31"}]->(f)
RETURN k,f

Create new Ferienhaus (returning variables is optional)
MATCH (s:Insel{name: "Madagaskar"})
CREATE (f:Ferienhaus {name: "testFerienhaus"})-[:STEHT_AUF]->(s)
RETURN s,f


READ scripts
------------

Get all
MATCH (n) RETURN n

Basic READ scripts
MATCH  (f:Ferienhaus{name:'Chaeshuette'})<-[g:GEBUCHT]-(k:Kunde) 
WHERE g.von = '01.02.2021'
RETURN f,k,g

MATCH  (f:Ferienhaus{name:'Chaeshuette'})<-[g:GEBUCHT]-(k:Kunde) 
WHERE date( g.von) < date('2021-03-01') 
RETURN f,k,g


Get node by ID
MATCH (n) 
WHERE ID(n) = 10 
RETURN n

Get node by range of ID
MATCH (n) 
WHERE ID(n) < 10 
AND ID(n) > 5 
RETURN n

Get node by name
MATCH (f:Ferienhaus{name: "Baumhaus"}) 
RETURN f

Get node with related nodes
MATCH (f:Ferienhaus {name: "Baumhaus"})<-[:GEBUCHT]-(k:Kunde) 
RETURN f,k


UPDATE Scripts
--------------

Update node by ID
MATCH (n) 
WHERE ID(n) = 10 
SET n.name = 'Updated Node' 
RETURN n

Update node by name
MATCH (k:Kunde {name: "testKunde"}) 
SET k.name = "Jenifer Gruber" 
RETURN k

Update relation
MATCH (k:Kunde {name: "Tom Bombadil"})-[r:GEBUCHT]->(f:Ferienhaus {name: "Jungelhuette"})
SET r.bis = "1989-05-22"
RETURN k,f


DELETE Scripts
--------------

Delete all
MATCH (n) DETACH DELETE n

Delete node by ID
MATCH (n)
WHERE ID(n) = 27
DELETE n

Delete node by ID including relationships
MATCH (n)
WHERE ID(n) = 10
DETACH DELETE n

Delete node by name
MATCH (k:Kunde {name: "Jenifer Gruber"})
DELETE k

Delete node by name including relationships
MATCH (k:Kunde {name: "Jenifer Gruber"})
DETACH DELETE k



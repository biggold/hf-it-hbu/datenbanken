﻿using System;
using System.Collections.Generic;
using Neo4j.Driver;
using Neo4J_Ferienhaus_Verwaltung.types;
using Newtonsoft.Json;

namespace Neo4J_Ferienhaus_Verwaltung
{
    public class CRUDFerienhaus : ICRUD<Ferienhaus>
    {
        private IDriver driver;
        private string database;
        public CRUDFerienhaus(IDriver driver, string database)
        {
            this.driver = driver;
            this.database = database;
        }

        public void Create(Ferienhaus obj)
        {
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                session.Run($"CREATE(f: Ferienhaus {{ name: '{obj.Name}'}})");
            }
        }

        public IList<Ferienhaus> ReadAll()
        {
            IList<Ferienhaus> ferienhaeuser = new List<Ferienhaus>();
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                var result = session.Run("MATCH (f:Ferienhaus) RETURN f");
                foreach (var record in result)
                {
                    var nodePropsFerienhaus = JsonConvert.SerializeObject(record[0].As<INode>().Properties);
                    Ferienhaus ferienhaus = JsonConvert.DeserializeObject<Ferienhaus>(nodePropsFerienhaus);
                    ferienhaus.ID = record[0].As<INode>().Id.ToString();

                    ferienhaeuser.Add(ferienhaus);
                }
            }
            return ferienhaeuser;
        }

        public Ferienhaus Read(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Ferienhaus obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }
    }
}

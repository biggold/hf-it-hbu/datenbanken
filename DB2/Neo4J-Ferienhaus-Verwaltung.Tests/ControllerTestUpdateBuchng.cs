using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    [TestClass]
    public class ControllerTestUpdateBuchung
    {
        private Controller ctrl;

        [TestInitialize]
        public void init()
        {
            ctrl = Helper.GetController();
        }

        [TestMethod]
        public void TestUpdateBuchung()
        {
            Gebucht neuGebucht = new Gebucht() {ID = "2", BisDate = DateTime.Now, Ferienhaus = new Ferienhaus("2", "Test3"), Kunde = new Kunde("2", "TestKunde3"), VonDate = DateTime.Now };
            ctrl.UpdateBuchung(neuGebucht);
            Gebucht createdGebucht = ctrl.GetBuchungen().Last();
            Assert.AreEqual(neuGebucht.BisDate, createdGebucht.BisDate);
        }
    }
}

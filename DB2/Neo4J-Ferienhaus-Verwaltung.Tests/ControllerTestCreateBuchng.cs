using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    [TestClass]
    public class ControllerTestCreateBuchung
    {
        private Controller ctrl;

        [TestInitialize]
        public void init()
        {
            ctrl = Helper.GetController();
        }

        [TestMethod]
        public void TestCreateBuchung()
        {
            Gebucht neuGebucht = new Gebucht() { BisDate = DateTime.Now, Ferienhaus = new Ferienhaus("3", "Test3"), Kunde = new Kunde("3", "TestKunde3"), VonDate = DateTime.Now };
            ctrl.CreateBuchung(neuGebucht);
            Gebucht createdGebucht= ctrl.GetBuchungen().Last();
            Assert.AreEqual(neuGebucht.BisDate, createdGebucht.BisDate);
        }
    }
}

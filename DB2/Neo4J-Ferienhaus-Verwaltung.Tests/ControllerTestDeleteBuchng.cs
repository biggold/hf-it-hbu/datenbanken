using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    [TestClass]
    public class ControllerTestDeleteBuchung
    {
        private Controller ctrl;

        [TestInitialize]
        public void init()
        {
            ctrl = Helper.GetController();
        }

        [TestMethod]
        public void TestDeleteBuchung()
        {
            string id = "1";
            ctrl.DeleteBuchung(id);
            Assert.ThrowsException<ObjectNotFoundException>(() => ctrl.GetBuchungByID(id));
        }
    }
}

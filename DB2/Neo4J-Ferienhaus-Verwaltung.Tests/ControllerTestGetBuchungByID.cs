using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    [TestClass]
    public class ControllerTestGetBuchungByID
    {
        private Controller ctrl;

        [TestInitialize]
        public void init()
        {
            ctrl = Helper.GetController();
        }

        [TestMethod]
        public void TestGetBuchungByID()
        {
            string id = "1";
            Gebucht buchungResult = ctrl.GetBuchungByID(id);
            Assert.AreEqual(id, buchungResult.ID);
        }

        [TestMethod]
        public void TestGetBuchungByID_WrongID()
        {
            string id = "10";
            Assert.ThrowsException<ObjectNotFoundException>(() => ctrl.GetBuchungByID(id));
        }
    }
}
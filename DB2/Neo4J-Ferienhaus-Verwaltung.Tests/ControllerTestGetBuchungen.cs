using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    [TestClass]
    public class ControllerTestGetBuchungen
    {
        private Controller ctrl;

        [TestInitialize]
        public void init()
        {
            ctrl = Helper.GetController();
        }

        [TestMethod]
        public void TestGetBuchungen()
        {
            IList<Gebucht> buchungen = ctrl.GetBuchungen();
            Assert.AreEqual(2, buchungen.Count);
        }
    }
}

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    [TestClass]
    public class ControllerTestGetBuchung
    {
        private Controller ctrl;

        [TestInitialize]
        public void init()
        {
            ctrl = Helper.GetController();
        }

        [TestMethod]
        public void TestGetBuchung()
        {
            Gebucht buchungSearch = new Gebucht("1");
            Gebucht buchungResult = ctrl.GetBuchung(buchungSearch);
            Assert.AreEqual(buchungSearch.ID, buchungResult.ID);
        }

        [TestMethod]
        public void TestGetBuchung_WrongID()
        {
            Gebucht buchungSearch = new Gebucht("10");
            Assert.ThrowsException<ObjectNotFoundException>(() => ctrl.GetBuchung(buchungSearch));
        }
    }
}

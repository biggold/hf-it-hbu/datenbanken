﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests.mock.cruds
{
    public class CRUDFerienhausMock : ICRUD<Ferienhaus>
    {
        private IList<Ferienhaus> kundeList = new List<Ferienhaus>();

        public CRUDFerienhausMock()
        {
            // Filling some data...
            kundeList.Add(new Ferienhaus("1", "TestFerienhaus1"));
            kundeList.Add(new Ferienhaus("2", "TestFerienhaus2"));
        }

        public void Create(Ferienhaus obj)
        {
            Ferienhaus kunde = new Ferienhaus(new Random().Next().ToString())
            {
                Name = obj.Name
            };
            kundeList.Add(kunde);
            //return kunde.ID;
        }

        public IList<Ferienhaus> ReadAll()
        {
            return kundeList;
        }

        public Ferienhaus Read(string id)
        {
            return kundeList.Single(kunde => kunde.ID.Equals(id));
        }

        public void Update(Ferienhaus obj)
        {
            // Remove old element
            var index = kundeList.IndexOf(kundeList.Single(kunde => kunde.ID.Equals(obj.ID)));
            kundeList.RemoveAt(index);
            // Add updated element
            kundeList.Add(obj);
        }

        public void Delete(string id)
        {
            var index = kundeList.IndexOf(kundeList.Single(kunde => kunde.ID.Equals(id)));
            kundeList.RemoveAt(index);
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests.mock.cruds
{
    public class CRUDKundeMock : ICRUD<Kunde>
    {
        private IList<Kunde> kundeList = new List<Kunde>();

        public CRUDKundeMock()
        {
            // Filling some data...
            kundeList.Add(new Kunde("1", "TestKunde1"));
            kundeList.Add(new Kunde("2", "TestKunde2"));
        }

        public void Create(Kunde obj)
        {
            Kunde kunde = new Kunde(new Random().Next().ToString())
            {
                Name = obj.Name
            };
            kundeList.Add(kunde);
            //return kunde.ID;
        }

        public IList<Kunde> ReadAll()
        {
            return kundeList;
        }

        public Kunde Read(string id)
        {
            return kundeList.Single(kunde => kunde.ID.Equals(id));
        }

        public void Update(Kunde obj)
        {
            // Remove old element
            var index = kundeList.IndexOf(kundeList.Single(kunde => kunde.ID.Equals(obj.ID)));
            kundeList.RemoveAt(index);
            // Add updated element
            kundeList.Add(obj);
        }

        public void Delete(string id)
        {
            var index = kundeList.IndexOf(kundeList.Single(kunde => kunde.ID.Equals(id)));
            kundeList.RemoveAt(index);
        }
    }
}

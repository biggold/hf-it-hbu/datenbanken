﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests.mock.cruds
{
    public class CRUDGebuchtMock : ICRUD<Gebucht>
    {
        private IList<Gebucht> gebuchtList = new List<Gebucht>();

        public CRUDGebuchtMock()
        {
            // Filling some data...
            gebuchtList.Add(new Gebucht("1") { BisDate = DateTime.Now, Ferienhaus = new Ferienhaus("1", "Test1"), Kunde = new Kunde("1", "TestKunde1"), VonDate = DateTime.Now });
            gebuchtList.Add(new Gebucht("2") { BisDate = DateTime.Now, Ferienhaus = new Ferienhaus("2", "Test2"), Kunde = new Kunde("2", "TestKunde2"), VonDate = DateTime.Now });
        }

        public void Create(Gebucht obj)
        {
            Gebucht gebucht = new Gebucht(new Random().Next().ToString())
            {
                BisDate = obj.BisDate,
                VonDate = obj.VonDate,
                Ferienhaus = obj.Ferienhaus,
                Kunde = obj.Kunde
            };
            gebuchtList.Add(gebucht);
            //return gebucht.ID;
        }

        public IList<Gebucht> ReadAll()
        {
            return gebuchtList;
        }

        public Gebucht Read(string id)
        {
            return gebuchtList.Single(gebucht => gebucht.ID.Equals(id));
        }

        public void Update(Gebucht obj)
        {
            // Remove old element
            var index = gebuchtList.IndexOf(gebuchtList.Single(gebucht => gebucht.ID.Equals(obj.ID)));
            gebuchtList.RemoveAt(index);
            // Add updated element
            gebuchtList.Add(obj);
        }

        public void Delete(string id)
        {
            var index = gebuchtList.IndexOf(gebuchtList.Single(gebucht => gebucht.ID.Equals(id)));
            gebuchtList.RemoveAt(index);
        }
    }
}

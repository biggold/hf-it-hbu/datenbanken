﻿using Neo4J_Ferienhaus_Verwaltung.Tests.mock.cruds;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung.Tests
{
    public static class Helper
    {
        public static Controller GetController()
        {
            ICRUD<Gebucht> gebuchtCrud = new CRUDGebuchtMock();
            ICRUD<Ferienhaus> ferienhausCrud = new CRUDFerienhausMock();
            ICRUD<Kunde> kundeCrud = new CRUDKundeMock();
            return new Controller(gebuchtCrud, kundeCrud, ferienhausCrud);
        }
    }
}

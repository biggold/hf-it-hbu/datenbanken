﻿
namespace Neo4J_Ferienhaus_Verwaltung
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBuchungen = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.buchungGridView = new System.Windows.Forms.DataGridView();
            this.panelInput = new System.Windows.Forms.Panel();
            this.newBuchungButton = new System.Windows.Forms.Button();
            this.newCustomerButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.neuerKundeTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.comboBoxHouse = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxKunde = new System.Windows.Forms.ComboBox();
            this.panelBuchungen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buchungGridView)).BeginInit();
            this.panelInput.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelBuchungen
            // 
            this.panelBuchungen.BackColor = System.Drawing.Color.DimGray;
            this.panelBuchungen.Controls.Add(this.label5);
            this.panelBuchungen.Controls.Add(this.buchungGridView);
            this.panelBuchungen.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBuchungen.Location = new System.Drawing.Point(0, 0);
            this.panelBuchungen.Name = "panelBuchungen";
            this.panelBuchungen.Size = new System.Drawing.Size(799, 282);
            this.panelBuchungen.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(360, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "Buchungen";
            // 
            // buchungGridView
            // 
            this.buchungGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.buchungGridView.BackgroundColor = System.Drawing.Color.DimGray;
            this.buchungGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.buchungGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buchungGridView.Location = new System.Drawing.Point(0, 46);
            this.buchungGridView.Name = "buchungGridView";
            this.buchungGridView.RowTemplate.Height = 25;
            this.buchungGridView.Size = new System.Drawing.Size(799, 236);
            this.buchungGridView.TabIndex = 0;
            this.buchungGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.buchungGridView_CellContentClick);
            this.buchungGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Update);
            this.buchungGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.SafeCurrentValue);
            this.buchungGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.buchungGridView_CellFormatting);
            // 
            // panelInput
            // 
            this.panelInput.AutoSize = true;
            this.panelInput.BackColor = System.Drawing.Color.Black;
            this.panelInput.Controls.Add(this.newBuchungButton);
            this.panelInput.Controls.Add(this.newCustomerButton);
            this.panelInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInput.Location = new System.Drawing.Point(0, 282);
            this.panelInput.Name = "panelInput";
            this.panelInput.Size = new System.Drawing.Size(799, 170);
            this.panelInput.TabIndex = 1;
            // 
            // newBuchungButton
            // 
            this.newBuchungButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.newBuchungButton.FlatAppearance.BorderSize = 0;
            this.newBuchungButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.newBuchungButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newBuchungButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.newBuchungButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.newBuchungButton.Location = new System.Drawing.Point(0, 46);
            this.newBuchungButton.Name = "newBuchungButton";
            this.newBuchungButton.Size = new System.Drawing.Size(799, 46);
            this.newBuchungButton.TabIndex = 4;
            this.newBuchungButton.Text = "Neuer Buchung";
            this.newBuchungButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newBuchungButton.UseVisualStyleBackColor = true;
            this.newBuchungButton.Click += new System.EventHandler(this.newBuchungButton_Click);
            // 
            // newCustomerButton
            // 
            this.newCustomerButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.newCustomerButton.FlatAppearance.BorderSize = 0;
            this.newCustomerButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.newCustomerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newCustomerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.newCustomerButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.newCustomerButton.Location = new System.Drawing.Point(0, 0);
            this.newCustomerButton.Name = "newCustomerButton";
            this.newCustomerButton.Size = new System.Drawing.Size(799, 46);
            this.newCustomerButton.TabIndex = 3;
            this.newCustomerButton.Text = "Neuer Kunde";
            this.newCustomerButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newCustomerButton.UseVisualStyleBackColor = true;
            this.newCustomerButton.Click += new System.EventHandler(this.newCustomerButton_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.dataGridView2);
            this.panel1.Controls.Add(this.neuerKundeTextBox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.saveButton);
            this.panel1.Controls.Add(this.dateTo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dateFrom);
            this.panel1.Controls.Add(this.comboBoxHouse);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBoxKunde);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(340, 282);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(459, 170);
            this.panel1.TabIndex = 2;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(307, 78);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(8, 8);
            this.dataGridView2.TabIndex = 10;
            // 
            // neuerKundeTextBox
            // 
            this.neuerKundeTextBox.Location = new System.Drawing.Point(6, 18);
            this.neuerKundeTextBox.Name = "neuerKundeTextBox";
            this.neuerKundeTextBox.Size = new System.Drawing.Size(198, 23);
            this.neuerKundeTextBox.TabIndex = 9;
            this.neuerKundeTextBox.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Name";
            this.label6.Visible = false;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(257, 129);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "Speichern";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // dateTo
            // 
            this.dateTo.Location = new System.Drawing.Point(256, 80);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(200, 23);
            this.dateTo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Bis";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Von";
            // 
            // dateFrom
            // 
            this.dateFrom.Location = new System.Drawing.Point(6, 80);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(200, 23);
            this.dateFrom.TabIndex = 4;
            // 
            // comboBoxHouse
            // 
            this.comboBoxHouse.FormattingEnabled = true;
            this.comboBoxHouse.Location = new System.Drawing.Point(256, 18);
            this.comboBoxHouse.Name = "comboBoxHouse";
            this.comboBoxHouse.Size = new System.Drawing.Size(141, 23);
            this.comboBoxHouse.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(256, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ferienhaus";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kunde";
            // 
            // comboBoxKunde
            // 
            this.comboBoxKunde.FormattingEnabled = true;
            this.comboBoxKunde.Location = new System.Drawing.Point(6, 18);
            this.comboBoxKunde.Name = "comboBoxKunde";
            this.comboBoxKunde.Size = new System.Drawing.Size(141, 23);
            this.comboBoxKunde.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 452);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelInput);
            this.Controls.Add(this.panelBuchungen);
            this.Name = "Form1";
            this.Text = "Buchungen";
            this.panelBuchungen.ResumeLayout(false);
            this.panelBuchungen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buchungGridView)).EndInit();
            this.panelInput.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelBuchungen;
        private System.Windows.Forms.DataGridView buchungGridView;
        private System.Windows.Forms.Panel panelInput;
        private System.Windows.Forms.Button newCustomerButton;
        private System.Windows.Forms.Button newBuchungButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxKunde;
        private System.Windows.Forms.ComboBox comboBoxHouse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TextBox neuerKundeTextBox;
    }
}


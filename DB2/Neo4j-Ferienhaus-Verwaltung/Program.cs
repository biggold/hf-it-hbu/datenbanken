using System;
using System.Windows.Forms;
using Neo4j.Driver;

namespace Neo4J_Ferienhaus_Verwaltung
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // docker run --publish=7474:7474 --publish=7687:7687 --env NEO4J_AUTH=neo4j/password --env NEO4J_ACCEPT_LICENSE_AGREEMENT=yes --env NEO4J_dbms_logs_query_enabled=true --env NEO4J_dbms_logs_query_parameter__logging__enabled=true neo4j
            var _driver = GraphDatabase.Driver("bolt://localhost:7687/", AuthTokens.Basic("testUser", "testUser"));

            var ctrl = new Controller(
                new CRUDGebucht(_driver, "ferienhaus"), 
                new CRUDKunde(_driver, "ferienhaus"),
                new CRUDFerienhaus(_driver, "ferienhaus")
                );

            Application.Run(new Form1(ctrl));
        }
    }
}

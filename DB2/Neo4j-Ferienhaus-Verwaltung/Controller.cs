﻿using System.Collections.Generic;
using Neo4J_Ferienhaus_Verwaltung.types;

namespace Neo4J_Ferienhaus_Verwaltung
{
    public class Controller
    {
        private ICRUD<Gebucht> gebuchtCrud;
        private ICRUD<Kunde> kundeCrud;
        private ICRUD<Ferienhaus> ferienhausCrud;

        public Controller(ICRUD<Gebucht> gebuchtCrud, ICRUD<Kunde> kundeCrud, ICRUD<Ferienhaus> ferienhausCrud)
        {
            this.gebuchtCrud = gebuchtCrud;
            this.kundeCrud = kundeCrud;
            this.ferienhausCrud = ferienhausCrud;
        }

        public IList<Gebucht> GetBuchungen()
        {
            return gebuchtCrud.ReadAll();
        }

        public Gebucht GetBuchung(Gebucht buchung)
        {
            return this.GetBuchungByID(buchung.ID);
        }

        public Gebucht GetBuchungByID(string id)
        {
            try
            {
                return gebuchtCrud.Read(id);
            }
            catch
            {
                throw new ObjectNotFoundException();
            }
        }

        public void CreateBuchung(Gebucht buchung)
        {
            gebuchtCrud.Create(buchung);
        }

        public void UpdateBuchung(Gebucht alteBuchung)
        {
            gebuchtCrud.Update(alteBuchung);
        }

        public void DeleteBuchung(string id)
        {
            gebuchtCrud.Delete(id);
        }

        public void CreateKunde(Kunde kunde)
        {
            kundeCrud.Create(kunde);
        }
        public IList<Kunde> GetKunden()
        {
            return kundeCrud.ReadAll();
        }

        public IList<Ferienhaus> GetFerienhaeuser()
        {
            return ferienhausCrud.ReadAll();
        }
    }
}

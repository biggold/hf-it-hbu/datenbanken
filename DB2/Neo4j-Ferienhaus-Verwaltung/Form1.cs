﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Neo4J_Ferienhaus_Verwaltung.types;
// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo
// ReSharper disable LocalizableElement

namespace Neo4J_Ferienhaus_Verwaltung
{
    public partial class Form1 : Form
    {
        //Inizialisierung
        private readonly Controller _controller;
        BindingList<Gebucht> _dataGridBuchung;
        private IList<Ferienhaus> _ferienhaus = new List<Ferienhaus>();
        private IList<Kunde> _kunden = new List<Kunde>();

        private string currentValue;

        public Form1(Controller cont)
        {
            InitializeComponent();
            _controller = cont;

            //Setup DataGrid View
            //
            //Set AutoGenerateColumns False
            buchungGridView.AutoGenerateColumns = false;
            buchungGridView.AllowUserToAddRows = false;

            //Set Columns Count
            buchungGridView.ColumnCount = 5;

            //Add Columns
            buchungGridView.Columns[0].Name = "ID";
            buchungGridView.Columns[0].HeaderText = "Buchungsnummer";
            buchungGridView.Columns[0].DataPropertyName = "ID";

            //Add Columns
            buchungGridView.Columns[1].Name = "Kunde";
            buchungGridView.Columns[1].HeaderText = "Kunde";
            buchungGridView.Columns[1].DataPropertyName = "Kunde";

            //Add Columns
            buchungGridView.Columns[2].Name = "Ferienhaus";
            buchungGridView.Columns[2].HeaderText = "Ferienhaus";
            buchungGridView.Columns[2].DataPropertyName = "Ferienhaus";

            //Add Columns
            buchungGridView.Columns[3].Name = "vonDate";
            buchungGridView.Columns[3].HeaderText = "Gebucht von";
            buchungGridView.Columns[3].DataPropertyName = "vonDate";

            //Add Columns
            buchungGridView.Columns[4].Name = "bisDate";
            buchungGridView.Columns[4].HeaderText = "Gebucht bis";
            buchungGridView.Columns[4].DataPropertyName = "bisDate";



            //Add Column Button
            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            buchungGridView.Columns.Add(btn);
            btn.HeaderText = "";
            btn.Text = "Löschen";
            btn.Name = "btn";
            btn.UseColumnTextForButtonValue = true;

            RefreshTable();
        }

        private void buchungGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                string EntityId = buchungGridView.Rows[e.RowIndex].Cells[0].Value.ToString();
                _controller.DeleteBuchung(EntityId);
                RefreshTable();
            }
        }
        
        private void buchungGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                var data = buchungGridView.Rows[e.RowIndex].DataBoundItem as Gebucht;
                if (data != null)
                {
                    buchungGridView.Rows[e.RowIndex].Cells["Kunde"].Value = data.Kunde.Name;
                    buchungGridView.Rows[e.RowIndex].Cells["Ferienhaus"].Value = data.Ferienhaus.Name;
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.Write(ex.Source);
            }
        }

        public void AddDataToCheckbox()
        {
            _ferienhaus = new List<Ferienhaus>();
            _kunden = new List<Kunde>();

            comboBoxHouse.DataSource = null;

            foreach (Ferienhaus a in _controller.GetFerienhaeuser())
            {
                _ferienhaus.Add(a);
            }

            foreach (Kunde kunde in _controller.GetKunden())
            {
                _kunden.Add(kunde);
            }
        }
        

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!neuerKundeTextBox.Visible)
            {
                Gebucht neueBuchung = new Gebucht(buchungGridView.Rows.Count.ToString(), (Kunde)comboBoxKunde.SelectedItem,
                    (Ferienhaus)comboBoxHouse.SelectedItem, dateFrom.Value, dateTo.Value);

                _controller.CreateBuchung(neueBuchung);
                RefreshTable();
            }
            else if(!String.IsNullOrEmpty(neuerKundeTextBox.Text))
            {
                String kundeId = (_kunden.Count + 1).ToString();
                _controller.CreateKunde(new Kunde(kundeId, neuerKundeTextBox.Text));
                RefreshTable();
                neuerKundeTextBox.Text = "";
            }
        }

        private void RefreshTable()
        {
             _dataGridBuchung = new BindingList<Gebucht>(_controller.GetBuchungen().OrderBy(o=>o.VonDate).ToList());
            
             
            buchungGridView.DataSource = _dataGridBuchung;
            buchungGridView.Refresh();
            
            AddDataToCheckbox();
            comboBoxKunde.DataSource = null;
            comboBoxHouse.DataSource = null;
            comboBoxHouse.DataSource = _ferienhaus;
            comboBoxKunde.DataSource = _kunden;
        }

        private void newCustomerButton_Click(object sender, EventArgs e)
        {
            VisableBuchung(false);
        }

        private void VisableBuchung(Boolean see)
        {
            label1.Visible = see;
            label2.Visible = see;
            label3.Visible = see;
            label4.Visible = see;
            comboBoxHouse.Visible = see;
            comboBoxKunde.Visible = see;
            dateFrom.Visible = see;
            dateTo.Visible = see;
            label6.Visible = !see;
            neuerKundeTextBox.Visible = !see;

        }

        private void newBuchungButton_Click (object sender, EventArgs e)
        {
            VisableBuchung(true);

        }

        private void SafeCurrentValue(object sender, DataGridViewCellEventArgs e)
        {
            currentValue = buchungGridView.CurrentCell.Value.ToString();
        }

        private void Update(object sender, DataGridViewCellEventArgs e)
        {
            //Small but unfortunately necessary hack bcs cellEndEdit fires at cell exit twice...
            string newValue = buchungGridView.CurrentCell.Value.ToString();

            if (newValue != currentValue)
            {
                string entityId = buchungGridView.Rows[e.RowIndex].Cells[0].Value.ToString();
                string entityKunde = buchungGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
                string entityFerienhaus = buchungGridView.Rows[e.RowIndex].Cells[2].Value.ToString();
                string entityvon = buchungGridView.Rows[e.RowIndex].Cells[3].Value.ToString();
                string entitybis = buchungGridView.Rows[e.RowIndex].Cells[4].Value.ToString();

                Gebucht neueBuchung = new Gebucht(entityId, new Kunde("TestID", entityKunde),
                    new Ferienhaus("TestID", entityFerienhaus), DateTime.Parse(entityvon), DateTime.Parse(entitybis));

                _controller.UpdateBuchung(neueBuchung);

                //Asynchronous Call that the Update Event is completed and then the refresh is called.
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    RefreshTable();
                }));
            }
        }
    }
}

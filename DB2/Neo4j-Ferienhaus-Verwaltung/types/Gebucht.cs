﻿using System;
using Newtonsoft.Json;

namespace Neo4J_Ferienhaus_Verwaltung.types
{
    public class Gebucht : Base
    {
        public Kunde Kunde;

        public Ferienhaus Ferienhaus;

        private DateTime vonDate;

        [JsonProperty("von")]
        public DateTime VonDate
        {
            get => vonDate;
            set => vonDate = value;
        }

        private DateTime bisDate;

        [JsonProperty("bis")]
        public DateTime BisDate
        {
            get => bisDate;
            set => bisDate = value;
        }

        public Gebucht() 
        {
            // Empty constructor for new objects
        }

        public Gebucht(string id) : base(id)
        {
            // Dumy function to forward to base constructor
        }

        public Gebucht(string id, Kunde kunde, Ferienhaus ferienhaus, DateTime vonDate, DateTime bisDate) : base(id)
        {
            this.vonDate = vonDate;
            this.bisDate = bisDate;
            this.Kunde = kunde;
            this.Ferienhaus = ferienhaus;
        }
    }
}

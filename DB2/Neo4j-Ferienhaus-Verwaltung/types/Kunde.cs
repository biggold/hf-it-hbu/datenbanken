﻿namespace Neo4J_Ferienhaus_Verwaltung.types
{
    public class Kunde : Base 
    {
        private string name;

        public string Name
        {
            get => name;
            set => name = value;
        }

        public Kunde()
        {
            // Empty constructor for new objects
        }

        public Kunde(string id) : base(id)
        {
            // Dumy function to forward to base constructor
        }

        public Kunde(string id, string name) : base(id)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return name;
        }
    }
}

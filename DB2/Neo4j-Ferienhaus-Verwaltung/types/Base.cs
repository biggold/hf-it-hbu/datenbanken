﻿namespace Neo4J_Ferienhaus_Verwaltung.types
{
    public abstract class Base
    {
        private string id;

        public string ID
        {
            get => id;
            set => id = value;
        }

        protected Base(string id)
        {
            this.id = id;
        }

        protected Base()
        {
            // Empty constructor for new objects
        }
    }
}

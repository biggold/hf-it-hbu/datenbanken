﻿namespace Neo4J_Ferienhaus_Verwaltung.types
{
    public class Ferienhaus : Base
    {
        private string name;

        public string Name
        {
            get => name;
            set => name = value;
        }
        public Ferienhaus()
        {
            // Empty constructor for new objects
        }
        public Ferienhaus(string id) : base(id)
        {
            // Dumy function to forward to base constructor
        }

        public Ferienhaus(string id, string name) : base(id)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return name;
        }
    }
}

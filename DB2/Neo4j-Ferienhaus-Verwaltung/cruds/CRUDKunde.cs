﻿using System;
using System.Collections.Generic;
using Neo4j.Driver;
using Neo4J_Ferienhaus_Verwaltung.types;
using Newtonsoft.Json;

namespace Neo4J_Ferienhaus_Verwaltung
{
    public class CRUDKunde : ICRUD<Kunde>
    {
        private IDriver driver;
        private string database;
        public CRUDKunde(IDriver driver, string database)
        {
            this.driver = driver;
            this.database = database;
        }

        public void Create(Kunde obj)
        {
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                var result = session.Run($"CREATE(k: Kunde {{ name: '{obj.Name}'}})");
            }
        }

        public IList<Kunde> ReadAll()
        {
            IList<Kunde> kunden = new List<Kunde>();
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                var result = session.Run("MATCH (k:Kunde) RETURN k");
                foreach (var record in result)
                {
                    var nodePropsKunde = JsonConvert.SerializeObject(record[0].As<INode>().Properties);
                    Kunde kunde = JsonConvert.DeserializeObject<Kunde>(nodePropsKunde);
                    kunde.ID = record[0].As<INode>().Id.ToString();

                    kunden.Add(kunde);
                }
            }

            return kunden;
        }

        public Kunde Read(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Kunde obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }
    }
}

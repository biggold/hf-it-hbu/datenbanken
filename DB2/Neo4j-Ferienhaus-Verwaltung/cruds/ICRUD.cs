﻿using System.Collections.Generic;

namespace Neo4J_Ferienhaus_Verwaltung
{
    public interface ICRUD<T>
    {
        public void Create(T obj);

        public IList<T> ReadAll();

        public T Read(string id);

        public void Update(T obj);

        public void Delete(string id);
    }
}

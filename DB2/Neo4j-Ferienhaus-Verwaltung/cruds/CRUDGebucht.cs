﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Neo4j.Driver;
using Neo4J_Ferienhaus_Verwaltung.types;
using Newtonsoft.Json;

namespace Neo4J_Ferienhaus_Verwaltung
{
    public class CRUDGebucht : ICRUD<Gebucht>
    {
        private IDriver driver;
        private string database;
        public CRUDGebucht(IDriver driver, string database)
        {
            this.driver = driver;
            this.database = database;
        }

        public void Create(Gebucht obj)
        {
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                session.Run($"MATCH(f: Ferienhaus), (k: Kunde) WHERE ID(f) = {obj.Ferienhaus.ID} AND ID(k) = {obj.Kunde.ID} CREATE(k) -[g: GEBUCHT {{ von: '{obj.VonDate.Date.ToString("yyyy-MM-dd")}', bis: '{obj.BisDate.Date.ToString("yyyy-MM-dd")}'}}]->(f)RETURN ID(g) as id");
            }
        }

        public IList<Gebucht> ReadAll()
        {
            IList<Gebucht> buchungen = new List<Gebucht>();
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                var result = session.Run("MATCH (k:Kunde)-[g:GEBUCHT]->(f:Ferienhaus) RETURN k,g,f");

                foreach (var record in result)
                {
                    // Serialize Gebucht (g)
                    var nodePropsGebucht = JsonConvert.SerializeObject(record[1].As<IRelationship>().Properties);
                    Gebucht buchung = JsonConvert.DeserializeObject<Gebucht>(nodePropsGebucht);
                    buchung.ID = record[1].As<IRelationship>().Id.ToString();

                    // Serialize Kunde (k)
                    var nodePropsKunde = JsonConvert.SerializeObject(record[0].As<INode>().Properties);
                    buchung.Kunde = JsonConvert.DeserializeObject<Kunde>(nodePropsKunde);
                    buchung.Kunde.ID = record[0].As<INode>().Id.ToString();

                    // Serialize Ferienhaus (f)
                    var nodeProps = JsonConvert.SerializeObject(record[2].As<INode>().Properties);
                    buchung.Ferienhaus = JsonConvert.DeserializeObject<Ferienhaus>(nodeProps);
                    buchung.Ferienhaus.ID = record[2].As<INode>().Id.ToString();

                    buchungen.Add(buchung);
                }
            }
            return buchungen;
        }

        public Gebucht Read(string id)
        {
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                var result = session.Run($"MATCH (k:Kunde)-[g:GEBUCHT]->(f:Ferienhaus) WHERE id(g)={id} RETURN k,g,f");
                try
                {
                    var record = result.First();

                    // Serialize Gebucht (g)
                    var nodePropsGebucht = JsonConvert.SerializeObject(record[1].As<IRelationship>().Properties);
                    Gebucht buchung = JsonConvert.DeserializeObject<Gebucht>(nodePropsGebucht);
                    buchung.ID = record[1].As<IRelationship>().Id.ToString();

                    // Serialize Kunde (k)
                    var nodePropsKunde = JsonConvert.SerializeObject(record[0].As<INode>().Properties);
                    buchung.Kunde = JsonConvert.DeserializeObject<Kunde>(nodePropsKunde);
                    buchung.Kunde.ID = record[0].As<INode>().Id.ToString();

                    // Serialize Ferienhaus (f)
                    var nodeProps = JsonConvert.SerializeObject(record[2].As<INode>().Properties);
                    buchung.Ferienhaus = JsonConvert.DeserializeObject<Ferienhaus>(nodeProps);
                    buchung.Ferienhaus.ID = record[2].As<INode>().Id.ToString();

                    return buchung;
                }
                catch
                {
                    throw new ObjectNotFoundException();
                }
            }
        }

        public void Update(Gebucht obj)
        {
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                Debug.WriteLine(obj.ID);
                Debug.WriteLine(obj.VonDate.Date.ToString("yyyy-MM-dd"));
                Debug.WriteLine(obj.BisDate.Date.ToString("yyyy-MM-dd"));
                Debug.WriteLine("kunde "+obj.Kunde.Name);
                Debug.WriteLine("ferienhaus "+obj.Ferienhaus.Name);
                session.Run(
                  $"MATCH(k:Kunde)-[g: GEBUCHT]->(f:Ferienhaus) " +
                  $"WHERE id(g)={obj.ID} " +
                  $"SET g.von = '{obj.VonDate.Date.ToString("yyyy-MM-dd")}', " +
                  $"g.bis = '{obj.BisDate.Date.ToString("yyyy-MM-dd")}', " +
                  $"k.name = '{obj.Kunde}' ," +
                  $"f.name = '{obj.Ferienhaus}' " +
                  $"RETURN g,k,f");
            }
        }

        public void Delete(string id)
        {
            using (var session = driver.Session(o => o.WithDatabase(database)))
            {
                session.Run($"MATCH (k:Kunde)-[g:GEBUCHT]->(f:Ferienhaus) WHERE id(g)={id} DELETE g");
            }
        }
    }
}
